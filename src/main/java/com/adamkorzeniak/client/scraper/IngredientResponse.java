package com.adamkorzeniak.client.scraper;

import lombok.Data;
import lombok.Generated;

import javax.validation.constraints.NotBlank;

@Data
@Generated
public class IngredientResponse {

    private Long id;

    @NotBlank
    private String name;

    private Double calories;

    private Double carbs;

    private Double fats;

    private Double proteins;

    private Double roughage;

    private Double salt;
}
