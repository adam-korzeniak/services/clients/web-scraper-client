package com.adamkorzeniak.client.scraper;

import com.adamkorzeniak.utils.configuration.YamlPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Valid
@Configuration
@ConfigurationProperties(prefix = "app.endpoint")
@PropertySource(value = "classpath:web-scraper-client.yml", factory = YamlPropertySourceFactory.class)
public class EndpointConfig {

    @NotNull
    private WebScraper webScraper;

    @Data
    @Valid
    public static class WebScraper {
        @NotBlank
        private String url;
        @NotBlank
        private String ileWazyPath;
    }
}
